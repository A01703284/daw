<?php
    $arreglo1 = array(16,2,3,56,8,46,3,2,35,7,9,10);
    $arreglo2 = array(5,43,61,4,6,7,9,12,46,23,14,34,2,3);
function imprimir($arreglo){
    foreach($arreglo as $valor){
        echo $valor;
        echo " , ";
    }
}
function promedio($arreglo){
    $promedio = array_sum($arreglo)/count($arreglo);
    echo $promedio;
}
function mediana($arreglo){
        $tam = count($arreglo);
        sort($arreglo);

        $arrmed = floor(($tam - 1) / 2);
        if($tam %  2 != 0){
            $med = $arreglo[$arrmed];
        }else{
            $med = ($arreglo[$arrmed] + ($arreglo[$arrmed + 1]))/2;
        }
        echo $med;
}
function todo($arreglo){
    echo "<p>El arreglo creado es: ";
    imprimir($arreglo);
    echo "</p>";
    echo "Promedio: ";
    promedio($arreglo);
    echo "<br>";
    echo "Mediana: ";
    mediana($arreglo);
    echo "<br>";
    echo "Orden ascendente: ";
    sort($arreglo);
    imprimir($arreglo);
    echo "<br>";
    echo "Orden descendente: ";
    rsort($arreglo);
    imprimir($arreglo);
}
function tabla(){ 
    echo '<table class="table table-dark">';
    for($i=0 ; $i <= 10; $i++){
        echo '<tr>';
        for($j = 1; $j <= 3; $j++){
            $num = pow($i,$j);
            echo '<td>' . $num . '</td>';        
        }
        echo '</tr>';   
    }
    echo '</table>';
    
}
function problema(){
    echo '<br/>6:<br/> Crea una solución para un problema de tu elección (puede ser algo relacionado con tus intereses, alguna problemática que hayas identificado en algún ámbito, un problema de programación que hayas
    resuelto en otro lenguaje, un problema de la ACM, entre otros). El problema debe estar descrito en un documento HTML, y la solución implementada en JavaScript, utilizando al menos la creación de un objeto,
    el objeto además de su constructor deben tener al menos 2 métodos. Muestra los resultados en el documento HTML.<br/><br/>';
    echo 'You have two variables a and b. Consider the following sequence of actions performed with these variables: 1. If a = 0 or b = 0, end the process. Otherwise, go to step 2; 2. If a ≥ 2·b, then set the value of a 
    to a - 2·b, and repeat step 1. Otherwise, go to step 3; 3. If b ≥ 2·a, then set the value of b to b - 2·a, and repeat step 1. Otherwise, end the process. Initially the values of a and b are positive integers, and 
    so the process will be finite.<br>';
    $a =12;
    $b =5;
    echo '<br>Valor = '.$a. '<br>Valor = '.$b;
}
?>