<?php
    function conectDb(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "cubi-13";
        $con = mysqli_connect($servername,$username,$password,$dbname);
        //Check connection
        if(!$con){
            die("Connection failed: ".mysqli_connect_error());
        }
        return $con;
    }
    function closeDb($mysql){
        mysqli_close($mysql);
    }
    function insertAlumno($a_nombre,$a_paterno,$a_materno,$peso,$a_sexo ,$a_nac,$a_inc,$a_com,$a_san,$a_med,$a_pol,$a_imss){
        $conn = conectDb();
        $index ="SELECT MAX(`id_alumno`) as indice from `alumno`";
        $row= mysqli_query($conn,$index);
        $numero=mysqli_fetch_object($row);
        $indice2 = $numero->indice;
        $indice2++;
        $sql ="INSERT INTO `alumno`(id_alumno, nombre, apellido_paterno, apellido_materno, peso, sexo, fecha_nacimiento, fecha_inclusion, comentarios, id_sangre, medicamentos, poliza_seguro, numero_imss, visible) VALUES ('$indice2','$a_nombre','$a_paterno','$a_materno','$peso','$a_sexo','$a_nac','$a_inc','$a_com','1','$a_med','$a_pol','$a_imss','1')";
       if(mysqli_query($conn,$sql)){
           echo "New record created successfully";
           closeDb($conn);
           return true;
       }else{
            echo "Error<br>".mysqli_error($conn);
            closeDb($conn);
           return false;
       }  
        closeDb($conn);
    }
    function insertTieneDiag($diagnosticos){
        $conn = conectDb();
        $index ="SELECT MAX(`id_alumno`) as indice from `alumno`";
        $row= mysqli_query($conn,$index);
        $numero=mysqli_fetch_object($row);
        $indice2 = $numero->indice;
        foreach($diagnosticos as $values){
            $query ="INSERT INTO `tiene_diagnosticos` (id_diagnostico,id_alumno) VALUES ('$values','$indice2')";
            if(mysqli_query($conn,$query)){
            }else{
                 echo "Error<br>".mysqli_error($conn);
            } 
        }
        
         closeDb($conn);
        
    }
    function insertTienealer($alergias){
        $conn = conectDb();
        $index ="SELECT MAX(`id_alumno`) as indice from `alumno`";
        $row= mysqli_query($conn,$index);
        $numero=mysqli_fetch_object($row);
        $indice2 = $numero->indice;
        foreach($alergias as $values){
            $query ="INSERT INTO `tiene_alergia` (id_alergia,id_alumno) VALUES ('$values','$indice2')";
            if(mysqli_query($conn,$query)){
            }else{
                 echo "Error<br>".mysqli_error($conn);
            } 
        }
        
         closeDb($conn);
        
    }
    include("forpadre.html");
?>
