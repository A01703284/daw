-- ---------------------------------------------------------------------------------------
-- Creacion de Clientes_Banca
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Clientes_Banca')
DROP TABLE Clientes_Banca
create table Clientes_Banca(
	NoCuenta varchar(5) not null primary key,
	Nombre varchar(30) not null,
	Saldo numeric(10,2) not null
)
-- ---------------------------------------------------------------------------------------
-- Creacion de Realizan
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Realizan')
DROP TABLE Realizan
Create table Realizan(
	NoCuenta varchar(5) not null,
	ClaveM varchar(2) not null,
	Fecha datetime not null,
	Monto numeric(10,2) not null
)
-- --------------------------------------------------------------------------------------
-- Creacion de Tipos_Movimiento
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Tipos_Movimiento')
DROP TABLE Tipos_movimiento
Create table Tipos_Movimiento(
	ClaveM varchar(2) primary key not null,
	Descripcion varchar(30) not null
)

	ALTER TABLE Realizan add constraint llaveRealizan PRIMARY KEY CLUSTERED (NoCuenta,ClaveM,Fecha)
  
	ALTER TABLE Realizan add constraint cfRealizan 
	foreign key (Nocuenta) references Clientes_Banca(NoCuenta); 

	ALTER TABLE Realizan add constraint cfRealizanm 
	foreign key (ClaveM) references Tipos_Movimiento(ClaveM);

	BEGIN TRANSACTION PRUEBA1 
	INSERT INTO Clientes_Banca VALUES('001', 'Manuel Rios Maldonado', 9000); 
	INSERT INTO Clientes_Banca VALUES('002', 'Pablo Perez Ortiz', 5000); 
	INSERT INTO Clientes_Banca VALUES('003', 'Luis Flores Alvarado', 8000); 
	COMMIT TRANSACTION PRUEBA1 

	SELECT * FROM CLIENTES_BANCA 
	ROLLBACK TRANSACTION PRUEBA2 

	BEGIN TRANSACTION PRUEBA3 
	INSERT INTO TIPOS_MOVIMIENTO VALUES('A','Retiro Cajero Automatico'); 
	INSERT INTO TIPOS_MOVIMIENTO VALUES('B','Deposito Ventanilla'); 
	COMMIT TRANSACTION PRUEBA3 

	BEGIN TRANSACTION PRUEBA4 
	INSERT INTO Realizan VALUES('001','A',GETDATE(),500); 
	UPDATE CLIENTES_BANCA SET SALDO = SALDO -500 
	WHERE NoCuenta='001' 
	COMMIT TRANSACTION PRUEBA4 

	SELECT * FROM Tipos_Movimiento
	SELECT * FROM CLIENTES_BANCA 
	SELECT * FROM Realizan

	BEGIN TRANSACTION PRUEBA5 
	INSERT INTO CLIENTES_BANCA VALUES('005','Rosa Ruiz Maldonado',9000);
	INSERT INTO CLIENTES_BANCA VALUES('006','Luis Camino Ortiz',5000); 
	INSERT INTO CLIENTES_BANCA VALUES('007','Oscar Perez Alvarado',8000); 


	IF @@ERROR = 0 
	COMMIT TRANSACTION PRUEBA5 
	ELSE 
	BEGIN 
	PRINT 'A transaction needs to be rolled back' 
	ROLLBACK TRANSACTION PRUEBA5 
	END 
-- -----------------------------------------------------------------------
-- REGISTRAR_RETIRO_CAJERO 
select * from CLIENTES_BANCA
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'REGISTRAR_RETIRO_CAJERO ' AND type = 'P')
                DROP PROCEDURE REGISTRAR_RETIRO_CAJERO 
            GO

            CREATE PROCEDURE REGISTRAR_RETIRO_CAJERO 
                @unocuenta varchar(5),
                @umonto numeric(10,2)
            AS
                UPDATE CLIENTES_BANCA SET SALDO = SALDO - @umonto
            GO
EXECUTE REGISTRAR_RETIRO_CAJERO  001,500
select * from CLIENTES_BANCA
-- -----------------------------------------------------------------------
-- REGISTRAR_RETIRO_CAJERO 
select * from CLIENTES_BANCA
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'REGISTRAR_DEPOSITO_VENTANILLA  ' AND type = 'P')
                DROP PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA  
            GO

            CREATE PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA  
                @unocuenta varchar(5),
                @umonto numeric(10,2)
            AS
                UPDATE CLIENTES_BANCA SET SALDO = SALDO + @umonto
            GO
EXECUTE REGISTRAR_DEPOSITO_VENTANILLA   001,500
select * from CLIENTES_BANCA