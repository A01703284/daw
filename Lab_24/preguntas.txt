Er�ndira Guadalupe Hern�ndez Garc�a	A01703284
�Que pasa cuando deseas realizar esta consulta?
Muestra las duplas que se ingresaron.

�Qu� pasa cuando deseas realizar esta consulta? 
No muestra nada en la consola es decir te bloquea toda la tabla.

Explica por qu� ocurre dicho evento.  
Porque como se esta modificando esa dupla solo la bloquea.

�Qu� ocurri� y por qu�? 
Detiene la transaccion del usuario 2.

�Para qu� sirve el comando @@ERROR revisa la ayuda en l�nea? 
Revisa que se haya cerrado la transaccion y sino la cierra.

�Qu� hace la transacci�n? 
Inserta nuevas duplas sin terminar la transaccion.

�Hubo alguna modificaci�n en la tabla? Explica qu� pas� y por qu�. 
Se agregaron las nuevas duplas