/*Borrado de elementos*/
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')
DROP TABLE Materiales 
CREATE TABLE Materiales 
( 
  Clave numeric(5) not null, 
  Descripcion varchar(50), 
  Costo numeric (8,2) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')
DROP TABLE Proveedores 
CREATE TABLE Proveedores 
( 
  RFC char(13) not null, 
  RazonSocial varchar(50) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')
DROP TABLE Proyectos 
CREATE TABLE Proyectos 
( 
  Numero numeric(5) not null, 
  Denominacion varchar(50) 
)
 
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')
DROP TABLE Entregan
CREATE TABLE Entregan 
( 
  Clave numeric(5) not null, 
  RFC char(13) not null, 
  Numero numeric(5) not null, 
  Fecha DateTime not null, 
  Cantidad numeric (8,2) 
) 
/*Carga de datos*/
BULK INSERT a1703284.a1703284.[Materiales] 
  FROM 'e:\wwwroot\a1703284\materiales.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT a1703284.a1703284.[Proyectos] 
  FROM 'e:\wwwroot\a1703284\proyectos.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n'
  ) 

BULK INSERT a1703284.a1703284.[Proveedores] 
  FROM 'e:\wwwroot\a1703284\proveedores.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n'
  ) 

SET DATEFORMAT dmy -- especificar formato de la fecha 

BULK INSERT a1703284.a1703284.[Entregan] 
  FROM 'e:\wwwroot\a1703284\entregan.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

 
/*Crea registro*/
insert into Materiales values (100,'xxx',1000)
/*Borrar registro*/
Delete from Materiales where Clave = 100 and Costo = 1000 
/*Creacion de un constraint*/
ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY (Clave) ;
INSERT INTO Materiales values(1000, 'xxx', 1000); 

sp_helpconstraint Materiales; 

	ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero) 
	ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
	ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY CLUSTERED (Clave, RFC, Numero,Fecha)
  
	ALTER TABLE Entregan add constraint cfentreganclave 
	foreign key (Clave) references Materiales(Clave); 

	ALTER TABLE Entregan add constraint cfentreganRFC 
	foreign key (RFC) references Proveedores(RFC);


	ALTER TABLE Entregan add constraint cfentregannumero 
	foreign key (Numero) references Proyectos(Numero);

	SELECT * from Materiales;
	SELECT * from Proyectos;
	SELECT * from Proveedores;
	SELECT * from Entregan; 

	ALTER TABLE entregan add constraint cantidad check (cantidad > 0) ;

	Select * from Entregan;