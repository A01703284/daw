/*Insercion tabla de Entrega*/
SET DATEFORMAT dmy

BULK INSERT a1703284.a1703284.Entrega
   FROM 'e:\wwwroot\a1703284\entregan.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      );
