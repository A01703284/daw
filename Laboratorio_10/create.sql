/*Tabla de materiales*/
CREATE TABLE Materiales
(
	  Clave numeric(5),
	  Descripcion varchar(50),
	  Costo numeric(8,2)
);
/*Tabla de proyectos*/
CREATE TABLE Proyectos
(
	  Numero numeric(5),
	  Denominacion varchar(50)
);
/*Tabla de Proveedores*/
CREATE TABLE Proveedores
(
	  RFC char(13),
	  RazonSocial varchar(50)
);
/*Tabla de Entrega*/
CREATE TABLE Entrega
(
  Clave numeric(5),
  RFC char(13),
  Numero numeric(5),
  Fecha datetime, 
  Cantidad numeric(8,2)

);