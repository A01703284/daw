/*Insercion tabla de proyectos*/
BULK INSERT a1703284.a1703284.[proyectos]
   FROM 'e:\wwwroot\a1703284\proyectos.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      );
