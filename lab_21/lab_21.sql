---------------------------------------------------------------
--CrearMaterial
select * from materiales

IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'creaMaterial' AND type = 'P')
                DROP PROCEDURE creaMaterial
            GO

            CREATE PROCEDURE creaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
            GO
EXECUTE creaMaterial 5000,'Martillos Acme',250,15 
select * from materiales
---------------------------------------------------------------
--ModificarMaterial
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modificarMaterial' AND type = 'P')
                DROP PROCEDURE modificarMaterial
            GO

            CREATE PROCEDURE modificarMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                update Materiales 
				set descripcion = @udescripcion,costo = @ucosto, porcentajeImpuesto = @uimpuesto
				where clave = @uclave
            GO
EXECUTE modificarMaterial 5000,'Martillos yu',230,10 
select * from materiales
---------------------------------------------------------------------
--DeleteMaterial
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'deleteMaterial' AND type = 'P')
                DROP PROCEDURE deleteMaterial
            GO

            CREATE PROCEDURE deleteMaterial
                @uclave NUMERIC(5,0)
            AS
                delete Materiales
				where clave = @uclave
            GO
EXECUTE deleteMaterial 5000
select * from materiales

---------------------------------------------------------------
--CrearProyecto
select * from Proyectos

IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'crearProyecto' AND type = 'P')
                DROP PROCEDURE crearProyecto
            GO

            CREATE PROCEDURE crearProyecto
                @unumero NUMERIC(5,0),
                @udenominacion VARCHAR(50)
            AS
                INSERT INTO Proyectos VALUES(@unumero, @udenominacion)
            GO
EXECUTE crearProyecto 3090,'Viva_Mexico'
select * from Proyectos
---------------------------------------------------------------
--ModificarProyectos
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modificarProyectos' AND type = 'P')
                DROP PROCEDURE modificarProyectos
            GO

            CREATE PROCEDURE modificarProyectos
                @unumero NUMERIC(5,0),
                @udenominacion VARCHAR(50)
            AS
                update Proyectos 
				set denominacion = @udenominacion
				where numero = @unumero
            GO
EXECUTE modificarProyectos 3090,'Tienda_yu'
select * from Proyectos
---------------------------------------------------------------------
--DeleteProyectos
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'deleteProyectos' AND type = 'P')
                DROP PROCEDURE deleteProyectos
            GO

            CREATE PROCEDURE deleteProyectos
                @unumero NUMERIC(5,0)
            AS
                delete Proyectos
				where numero = @unumero
            GO
EXECUTE deleteProyectos 3090
select * from Proyectos

---------------------------------------------------------------
--CrearProveedores
select * from Proveedores

IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'crearProveedores' AND type = 'P')
                DROP PROCEDURE crearProveedores
            GO

            CREATE PROCEDURE crearProveedores
                @uRFC char(13),
                @uRazonSocial VARCHAR(50)
            AS
                INSERT INTO Proveedores VALUES(@uRFC, @uRazonSocial)
            GO
EXECUTE crearProveedores'IIII800102','La_tiendita'
select * from Proveedores
---------------------------------------------------------------
--ModificarProveedores
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modificarProveedores' AND type = 'P')
                DROP PROCEDURE modificarProveedores
            GO

            CREATE PROCEDURE modificarProveedores
                @uRFC char(13),
                @uRazonSocial varchar(50)
            AS
                update Proveedores 
				set RazonSocial = @uRazonSocial
				where RFC = @uRFC
            GO
EXECUTE modificarProveedores IIII800102,'Tienda_yu'
select * from Proveedores
---------------------------------------------------------------------
--DeleteProveedores
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'deleteProveedores' AND type = 'P')
                DROP PROCEDURE deleteProveedores
            GO

            CREATE PROCEDURE deleteProveedores
                @uRFC char(13)
            AS
                delete Proveedores
				where RFC = @uRFC
            GO
EXECUTE deleteProveedores IIII800102
select * from Proveedores

---------------------------------------------------------------
--CrearEntregan
select * from Entregan
SET DATEFORMAT dmy
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'crearEntregan' AND type = 'P')
                DROP PROCEDURE crearEntregan
            GO

            CREATE PROCEDURE crearEntregan
				@uclave numeric (5,0),
                @uRFC char(13),
				@unumero numeric(5,0),
				@ufecha datetime,
				@ucantidad numeric (8,2)
            AS
                INSERT INTO Entregan 
				VALUES(@uclave,@uRFC,@unumero,@ufecha,@ucantidad)
            GO
EXECUTE crearEntregan 1000,'AAAA800101',5000,'1998-07-09 00:00:00.000',146
select * from Entregan
---------------------------------------------------------------
--ModificarEntregan
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'modificarEntregan' AND type = 'P')
                DROP PROCEDURE modificarEntragan
            GO

            CREATE PROCEDURE modificarEntregan
				@uClave numeric(5),
                @ufecha datetime,
				@uCantidad numeric(8,2)
            AS
                update Entregan 
				set Cantidad = @uCantidad
				where Clave = @uClave and Fecha = @uFecha
            GO
EXECUTE modificarEntregan 1000,'1998-07-08 00:00:00.000',180
select * from Entregan
---------------------------------------------------------------------
--DeleteEntregan
IF EXISTS (SELECT name FROM sysobjects
                       WHERE name = 'deleteEntregan' AND type = 'P')
                DROP PROCEDURE deleteEntregan
            GO

            CREATE PROCEDURE deleteEntregan
				@uFecha datetime
            AS
                delete Entregan
				where Fecha = @uFecha
            GO
EXECUTE deleteEntregan '1998-07-08 00:00:00.000'
select * from Entregan
