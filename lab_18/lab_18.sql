
/*La suma de las cantidades e importe total de todas las entregas realizadas durante el 97. */
SET DATEFORMAT DMY
SELECT SUM(e.Cantidad) AS 'Suma', SUM (e.Cantidad*((m.Costo*((m.PorcentajeImpuesto/100)+1)))) AS 'Total'
FROM Entregan AS e, Materiales AS m
WHERE m.Clave=e.Clave AND (E.Fecha BETWEEN '01/01/1997' AND '31/12/1997')
/*Resultado:
Suma cantidades  importe total
546               44789.472
*/


/*Para cada proveedor, obtener la raz�n social del proveedor, n�mero de entregas e importe total de las entregas realizadas. */
SELECT p.RazonSocial, COUNT(e.Cantidad) AS 'N. de entregas', SUM (((m.Costo*((m.PorcentajeImpuesto/100)+1)))*e.Cantidad) AS 'importe total'
FROM Proveedores AS p, Entregan AS e, Materiales AS m
WHERE p.RFC=e.RFC AND m.Clave=e.Clave
GROUP BY p.RazonSocial
/*Resultado:
RazonSocial				n�mero de entregas  importe total
Alvin					15					794295.2064000000
Cecoferre				18					860441.9770000000
Comex					15					801930.5980000000
La Ferre				18					919531.4300000000
La fragua				20					2331459.6920000000
Oviedo					18					1059888.7370000000
Tabiquera del centro	15					1937798.7240000000
Tubasa					15					2278398.9930000000
*/



/*Por cada material obtener la clave y descripci�n del material, la cantidad total entregada,la m�nima cantidad entregada, la m�xima cantidad entregada,
el importe total de las entregas de aquellos materiales en los que la cantidad promedio entregada sea mayor a 400. */

SELECT m.Clave, m.Descripcion, SUM(e.Cantidad) AS 'Total entregada', MIN(e.Cantidad) AS 'Cantidad minima entregada', MAX(e.Cantidad) AS 'Cantidad maxima entregada', SUM(((m.Costo*((m.PorcentajeImpuesto/100)+1)))*e.Cantidad) AS 'Importe total'
FROM Materiales AS m, Entregan AS e
where m.Clave=e.Clave
GROUP BY m.Clave, m.Descripcion
HAVING AVG(e.Cantidad)>400
/*Respuesta:
Clave	Descripcion			Total entregada		Cantidad minima entregada	Cantidad maxima entregada	importe total
1010	Varilla 4/32		1718.00				523.00						667.00						201560.9140000000
1040	Varilla 3/18		1349.00				263.00						546.00						220329.4720000000
1050	Varilla 4/34		1216.00				90.00						623.00						217268.8000000000
1080	Ladrillos rojos		1214.00				86.00						699.00						62011.1200000000
1100	Block				1688.00				466.00						699.00						51754.0800000000
1130	Sillar gris			1298.00				63.00						673.00						146006.8280000000
1140	Cantera blanca		1453.00				219.00						651.00						297225.6800000000
1200	Recubrimiento P1019	1415.00				177.00						653.00						347750.4000000000
1220	Recubrimiento P1037	1335.00				24.00						658.00						382920.7200000000
1250	Grava				1452.00				71.00						691.00						148830.0000000000
1270	Tezontle			1376.00				324.00						546.00						112876.0320000000
1320	Tuber�a 4.4			1274.00				163.00						698.00						300755.7280000000
1330	Tuber�a 3.7			1205.00				93.00						558.00						296892.7200000000
1410	Pintura B1021		1529.00				461.00						601.00						196514.7250000000
1420	Pintura C1012		1325.00				278.00						603.00						170328.7500000000
*/


/*Para cada proveedor, indicar su raz�n social y mostrar la cantidad promedio de cada material entregado, detallando la clave y descripci�n del 
material, excluyendo aquellos proveedores para los que la cantidad promedio sea menor a 500.*/
SELECT p.RazonSocial, AVG(e.Cantidad) AS 'Promedio de material', m.Clave, m.Descripcion
FROM Proveedores AS p,Materiales AS m, Entregan AS e
WHERE p.RFC=E.RFC AND m.Clave=e.Clave
GROUP BY p.RazonSocial, m.Clave, m.Descripcion
HAVING AVG(e.Cantidad) >= 500
/*Respuesta:
RazonSocial		Promedio de material	Clave	Descripcion
Oviedo			572.666666				1010	Varilla 4/32
La Ferre		562.666666				1100	Block
Oviedo			509.666666				1410	Pintura B1021
*/

/*Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos grupos de proveedores: aquellos para los que la cantidad 
promedio entregada es menor a 370 y aquellos para los que la cantidad promedio entregada sea mayor a 450.*/
SELECT p.RazonSocial, AVG(e.Cantidad) AS 'Promedio de material', m.Clave, m.Descripcion
FROM Proveedores AS p,Materiales AS m, Entregan AS e
WHERE p.RFC=E.RFC AND m.Clave=e.Clave
GROUP BY p.RazonSocial, m.Clave, m.Descripcion
HAVING AVG(e.Cantidad) < 370 OR AVG(e.Cantidad) > 450

/*Resulatado:
RazonSocial				Promedio de material	Clave	Descripcion
La fragua				143.600000				1000	Varilla 3/16
Oviedo					572.666666				1010	Varilla 4/32
La Ferre				356.000000				1020	Varilla 3/17
Cecoferre				212.000000				1030	Varilla 4/33
Tabiquera del centro	354.333333				1060	Varilla 3/19
Tubasa					340.333333				1070	Varilla 4/35
Oviedo					368.666666				1090	Ladrillos grises
La Ferre				562.666666				1100	Block
Cecoferre				332.333333				1110	Megablock
Alvin					358.000000				1120	Sillar rosa
Son 34 duplas asi que no las puse todas
*/

/*Utilizando la sentencia insert into*/

INSERT INTO Materiales VALUES (1440, 'Granito', 1240, 15.5);
INSERT INTO Materiales VALUES (1450, 'Cemento', 79, 7.8);
INSERT INTO Materiales VALUES (1460, 'Vidrio', 100, 2.3);
INSERT INTO Materiales VALUES (1470, 'Caucho', 1000, 5.6);
INSERT INTO Materiales VALUES (1480, 'Acrilicos', 150, 12.2);

SELECT * FROM Materiales


/*Clave y descripci�n de los materiales que nunca han sido entregados. */
SELECT m.Clave, m.Descripcion
FROM Materiales AS m
WHERE m.Clave NOT IN (
SELECT e.Clave
FROM Entregan AS e)

/*Resultado:
Clave	Descripcion
1440	Granito
1450	Cemento
1460	Vidrio
1470	Caucho
1480	Acrilicos
3442	Tornillo
*/

/*Raz�n social de los proveedores que han realizado entregas tanto al proyecto 'Vamos M�xico' como al proyecto 'Quer�taro Limpio'. */

SELECT Prov.RazonSocial
FROM Proveedores AS Prov, Proyectos AS P
WHERE Prov.RazonSocial IN(
  SELECT Prov.RazonSocial
  FROM Proveedores AS Prov
  WHERE Prov.RazonSocial='Vamos M�xico'
) AND Prov.RFC IN(
  SELECT Prov.RazonSocial
  FROM Proveedores AS Prov
  WHERE Prov.RazonSocial='Quer�taro Limpio'
)
 /*SALIDA:
RazonSocial
La fragua
*/

/*Descripci�n de los materiales que nunca han sido entregados al
proyecto 'CIT Yucat�n'. */

SELECT M.Descripcion
FROM Materiales AS M
WHERE M.Clave NOT IN
      (
  SELECT E.Clave
  FROM Proyectos AS P, Entregan AS E
  WHERE P.Numero=E.Numero AND P.Denominacion LIKE 'CIT Yucatan'
)
  SELECT *FROM Materiales
select * from Proyectos p, entregan e where e.Numero=p.Numero
  /*SALIDA:
Muestra:
Varilla 3/16
Varilla 4/32
Varilla 3/17
Varilla 4/33
Varilla 4/34
Varilla 3/19
Varilla 4/35
*/

/*Raz�n social y promedio de cantidad entregada de los proveedores
cuyo promedio de cantidad entregada es mayor al promedio de la cantidad
entregada por el proveedor con el RFC 'VAGO780901'. */

SELECT Prov.RazonSocial, AVG(E.Cantidad) AS 'Promedio de cantidad entregada'
FROM Entregan AS E, Proyectos AS P,Proveedores AS Prov
WHERE P.Numero=E.Numero AND Prov.RFC=E.RFC
GROUP BY Prov.RazonSocial
HAVING AVG(E.Cantidad)> (SELECT AVG(E.Cantidad)
  FROM Entregan AS E
  WHERE E.RFC='VAGO780901'
)
  /*SALIDA:
RazonSocial  Promedio de cantidad entregada
*/

/*RFC, raz�n social de los proveedores que participaron en el proyecto
'Infonavit Durango' y cuyas cantidades totales entregadas en el 2000
fueron mayores a las cantidades totales entregadas en el 2001.*/

set dateformat dmy
SELECT Prov.RFC, Prov.RazonSocial
FROM Entregan AS E, Proyectos AS P,Proveedores AS Prov
WHERE P.Numero=E.Numero AND Prov.RFC=E.RFC and Prov.RazonSocial='Infonavit Durango'
GROUP BY Prov.RFC, Prov.RazonSocial
  HAVING (SELECT SUM(E.Cantidad)
  FROM Entregan AS E, Proyectos AS P,Proveedores AS Prov
  WHERE P.Numero=E.Numero AND Prov.RFC=E.RFC AND (E.Fecha BETWEEN '01/01/2000' AND '31/12/2000')
)
  > (SELECT SUM(E.Cantidad)
  FROM Entregan AS E, Proyectos AS P,Proveedores AS Prov
  WHERE P.Numero=E.Numero AND Prov.RFC=E.RFC AND (E.Fecha BETWEEN '01/01/2001' AND '31/12/2001')
)

/*SALIDA:
RFC RazonSocial
*/