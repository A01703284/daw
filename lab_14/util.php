<?php
    function conectDb(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "cubi-13";

        $con = mysqli_connect($servername,$username,$password,$dbname);
         //Check connection
         if(!$con){
             die("Connection failed: ".mysqli_connect_error());
         }
         return $con;
    }
    function closeDb($mysql){
        mysqli_close($mysql);
    }
    function getInformacion(){
        $conn = conectDb();
        $sql = "SELECT * FROM alumno";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return $result;
    }
    function getAlumnoByName($alumno_name){
        $conn = conectDb();
        $sql = "SELECT * FROM alumno WHERE nombre LIKE '%".$alumno_name."%'";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return $result;
    }
    function getmenorpeso($peso_alumno){
        $conn = conectDb();
        $sql = "SELECT * FROM alumno WHERE peso <= '".$peso_alumno."'";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return $result;
    }
?>
