<?php
    require_once("util.php");
    session_start();
    
    if(isset($_POST["email"]) && isset($_POST["password"])) {
        $usuario = login(htmlspecialchars($_POST["email"]), htmlspecialchars($_POST["password"]));
        echo $usuario;
        if ($usuario != "") {
            $_SESSION["usuario"] = $usuario;
            header("location:index.php");
        } else {
            $error = "Usuario y/o contraseña incorrectos";
            include("index.html");
        }
    } else {
        $error = "Usuario y/o contraseña incorrectos";
        include("_index.html");
    }
?>
